import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Globals } from 'app/globals';


@Injectable()
export class AuthenticationService {

    constructor(private http: Http, private router: Router, private globals: Globals) { }
    
    login(username: string, password: string): Observable<boolean> {
        let body = 'userName=' + username + '&password=' + password + '&grant_type=password';
        let cabecera = new Headers();
        cabecera.append('Content-Type', 'application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: cabecera });

        return this.http.post(this.globals.urlpath + 'token', body, options)
            .map((response: Response) => {
                let token = response.json() && response.json().access_token;
                let nombre = response.json() && response.json().Nombre;
                let apellido = response.json() && response.json().Apellido;
                let isOwner = (response.json().IsOwner == "True");
                let empresa = response.json() && response.json().Empresa;
                if (token) {

                    localStorage.setItem('currentUser', JSON.stringify({
                        username: username, token: token,
                        Apellido: apellido, Nombre: nombre, IsOwner: isOwner, Empresa: empresa
                    }));
                    this.router.navigate(['/formulario']);
                } else {
                    return false;
                }
            });
    }
    olvidoClave(email: string): Observable<boolean> {

        let body = 'email=' + email;
        let cabecera = new Headers();
        cabecera.append('Content-Type', 'application/x-www-form-urlencoded');
        let options = new RequestOptions({ headers: cabecera });
        return this.http.post(this.globals.urlpathApi + 'Account/ForgotPassword', body, options)
            .map((response: Response) => {
                return true;
            });
    }
    restaurarClave(restaurar: any): Observable<boolean> {

        const headers = new Headers(
            {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json; charset=utf-8"
            });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.globals.urlpathApi + 'Account/ResetPassword', restaurar, options)
            .map((response: Response) => {
                return true;
            });
    }
}