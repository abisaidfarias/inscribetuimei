import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Globals } from 'app/globals';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';


@Injectable()
export class ImeiInfoService {

    constructor(private http: Http, private router: Router, private globals: Globals) { }
    //GET
    getInfoClientes(desde:Date,hasta:Date, status:number): Observable<boolean> {
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        const headers = new Headers(
            {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json; charset=utf-8",
                "Authorization": "Bearer " + curentUser.token
            });

        return this.http.get(this.globals.urlpathApi + 'Imei/GetImeiInfo?desde=' + desde +
            '&hasta='+hasta+'&status='+status, { headers: headers })
            .map((response: Response) => { return response.json() });
    }
    ImeiCheck(imeiCheck: any): Observable<boolean> {
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        const headers = new Headers(
            {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json; charset=utf-8",
                "Authorization": "Bearer " + curentUser.token
            });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.globals.urlpathApi + 'Imei/ImeiCheck', imeiCheck, options)
        .map((response: Response) => {
            return true;
        });
    }
}