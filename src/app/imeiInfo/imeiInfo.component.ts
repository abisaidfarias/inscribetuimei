import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ImeiInfoService } from './imeiInfo.service';
import swal from 'sweetalert2';
import { LinkRutUrlComponent } from 'app/components/buttons/linkRutUrl.component';
import { LinkImeiElectronicComponent } from 'app/components/buttons/linkImeiElectronic.component';
import { LinkImeiPhysicComponent } from 'app/components/buttons/linkImeiPhysic.component';
import { LinkStampedComponent } from 'app/components/buttons/linkStamped.component';
import { LinkReceiptComponent } from 'app/components/buttons/linkReceipt.component';
import { ClientOptionsComponent } from 'app/components/buttons/clientOptions.component';




declare const $: any;

class ImeiCheck {
    ImeiRegisterId: number;
    Status: number;
    Mensaje: string;
}


@Component({
    selector: 'app-imeiInfo',
    templateUrl: './imeiInfo.component.html'
})

export class ImeiInfoComponent implements OnInit {

    constructor(private router: Router, private formBuilder: FormBuilder, private imeiInfoService: ImeiInfoService) { }
    
    public data: any;
    public desde:any;
    public hasta:any;
    public imeiStatus:number=0;

    settings = {
        defaultStyle: false,
        pager: {
            display: true,
            perPage: 50
          },
        actions: false,
        columns: {
            Id: {
                title: 'Id',
                width: '200px'
            },
            Name: {
                title: 'Name',
                width: '200px'
            },
            Rut: {
                title: 'Rut',
                width: '180px'
            },
            Email: {
                title: 'Email',
                width: '150px'
            },
            Phone: {
                title: 'Phone',
                width: '150px'
            },
            Brand: {
                title: 'Brand',
                width: '150px'
            },
            CommercialModel: {
                title: 'Model',
                width: '150px'
            },
            PrimerImei: {
                title: 'IMEI #1',
                width: '150px'
            },
            SegundoImei: {
                title: 'IMEI #2',
                width: '150px'
            },
            Description: {
                title: 'Comment',
                width: '150px'
            },
            PhoneOrigin: {
                title: 'Pais origen',
                width: '150px'
            },
            CreatedDate:{
                title: 'Fecha Solicitud',
                width: '150px'
            },
            UrlImageRutPassport: {
                title: 'Imagen Rut',
                type:'custom',
                width: '150px',
                renderComponent: LinkRutUrlComponent
            },
            UrlImageElectronicImei: {
                title: 'IMEI Electrónico',
                width: '150px',
                renderComponent: LinkImeiElectronicComponent,
                type: 'custom',
            },
            UrlImagePhysicalImei: {
                title: 'IMEI Fisico',
                width: '150px',
                renderComponent: LinkImeiPhysicComponent,
                type: 'custom',
            },
            UrlImageStampedPassport: {
                title: 'Pasaporte Estampado',
                width: '150px',
                renderComponent: LinkStampedComponent,
                type: 'custom',
            },
            UrlImageReceipt: {
                title: 'Certificado de Compra',
                width: '150px',
                renderComponent: LinkReceiptComponent,
                type: 'custom',
            },
            Status: {
                title: 'Status',
                width: '150px'
            },
            Opciones:{
                filter: false,
                title: 'Opciones',
                type: 'custom',
                width: '200px',
                renderComponent: ClientOptionsComponent,
                onComponentInitFunction: (instance) => {
                    console.log(instance);
                    instance.save.subscribe(row => {
                        if (row.Type == "revisado") {
                            this.popUpCheckImei(row.Id,2);
                        } else if (row.Type == "rechazado") {
                            this.popUpCheckImei(row.Id, 1);
                        } else {
                            this.popUpCheckImei(row.Id, 3);
                        }
                    });
                }
            }
        },
        rowClassFunction: (row) => {
            if(row.data.Atrasado)
            {
                return 'highlightOwner';
            }
            return '';
        }
    };

    popUpCheckImei(idRegisterId, status){
        let htmlBody = "";
        if(status==2){
            htmlBody = '<div class="form-group">' +
                '<div class="row">' +
                '<h4><strong>Esta seguro que desea aprobar esta inscripción?</strong></h4>' +
                '</div>' +
                '</div>';
        }
        else if(status == 1){
            htmlBody = '<div class="form-group">' +
                '<div class="row">' +
                '<h4><strong>Rechazar Inscripción</strong></h4>' +
                '</div>' +
                '<div class="row">' +
                '<textarea id="mensaje" class="form-control" rows = "5">Se debe escribir la causas del rechazo.</textarea>' +
                '</div>' +
                '</div>';
        }else{
            htmlBody = '<div class="form-group">' +
                '<div class="row">' +
                '<h4><strong>Esta seguro que desea DESCARTAR esta inscripción?</strong></h4>' +
                '</div>' +
                '</div>';
        }
        swal({
            title: "",
            html: htmlBody,
            showCancelButton: false,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false,
            width: "500px"
        }).then((result) => {
            if ($('#mensaje').val()){
                this.imeiCheck(idRegisterId, status, $('#mensaje').val());
            }else{
                this.imeiCheck(idRegisterId, status,'');
            }
        })
    }
    imeiCheck(idRegisterId,status,mensaje){

        let imeiCheck = new ImeiCheck();
        imeiCheck.ImeiRegisterId=idRegisterId;
        imeiCheck.Status=status;
        imeiCheck.Mensaje=mensaje;
        this.imeiInfoService.ImeiCheck(imeiCheck)
        .subscribe(
            (servers: any) => {
                this.get();
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Error en la Operación</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
        });

    }
    ngOnInit() {
        var today = new Date();
        var dateNow = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        let newDate = new Date(dateNow);
        this.desde=newDate.toISOString().slice(0, 10);;
        this.hasta=newDate.toISOString().slice(0, 10);;
        this.get();
        $('#principal').css("width", "1600");
    }
    get() {
        swal({
            title: 'Consultando...',
            allowOutsideClick: false
        });
        swal.showLoading();
        this.imeiInfoService.getInfoClientes(this.desde,this.hasta,this.imeiStatus)
        .subscribe(
            (servers: any) => {
                this.data = servers;
                swal.close();
            },
            (error) => {
                swal.close();
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Error en la Operación</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
        });

    }
}
