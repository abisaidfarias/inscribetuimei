import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



import { Globals } from 'app/globals';
import { RecaptchaModule } from 'ng-recaptcha';
import { ImeiInfoComponent } from './imeiInfo.component';
import { ImeiInfoService } from './imeiInfo.service';
import { ImeiInfoRoutes } from './imeiInfo.routing';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { LinkRutUrlComponent } from 'app/components/buttons/linkRutUrl.component';
import { LinkImeiElectronicComponent } from 'app/components/buttons/linkImeiElectronic.component';
import { LinkImeiPhysicComponent } from 'app/components/buttons/linkImeiPhysic.component';
import { LinkStampedComponent } from 'app/components/buttons/linkStamped.component';
import { LinkReceiptComponent } from 'app/components/buttons/linkReceipt.component';
import { ClientOptionsComponent } from 'app/components/buttons/clientOptions.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ImeiInfoRoutes),
        FormsModule,
        ReactiveFormsModule,
        RecaptchaModule.forRoot(),
        Ng2SmartTableModule
    ],
    declarations: [ImeiInfoComponent, ClientOptionsComponent, LinkRutUrlComponent,LinkImeiElectronicComponent,LinkImeiPhysicComponent,LinkStampedComponent,LinkReceiptComponent],
    entryComponents: [LinkRutUrlComponent, ClientOptionsComponent,  LinkImeiElectronicComponent, LinkImeiPhysicComponent, LinkStampedComponent, LinkReceiptComponent],
    providers: [ImeiInfoService, Globals]
})

export class ImeiInfoModule { }
