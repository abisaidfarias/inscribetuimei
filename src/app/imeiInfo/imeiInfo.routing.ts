import { Routes } from '@angular/router';

import { ImeiInfoComponent } from './imeiInfo.component';

export const ImeiInfoRoutes: Routes = [
    {

        path: '',
        children: [{
            path: 'imeiInfo',
            component: ImeiInfoComponent
        }]
    }
];
