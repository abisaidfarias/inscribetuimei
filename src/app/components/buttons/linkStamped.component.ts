import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';



@Component({

    selector: 'button-view',
    templateUrl: 'linkStamped.component.html'
})

export class LinkStampedComponent implements ViewCell, OnInit {


    public imagenUrl: string;
    public labelUrl: string;
    renderValue: string;
    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {

        this.imagenUrl = this.rowData.UrlImageStampedPassport;
        if (this.imagenUrl != null) {
            this.labelUrl = "Imagen Pasaporte Estampado";
        }
        else {
            this.labelUrl = "";
        }
        if (this.value != null) {
            this.renderValue = this.value.toString().toUpperCase();
        }

    }

    onClick() {
        this.save.emit(this.rowData);
    }
}