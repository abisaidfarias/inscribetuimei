import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-view',
    template: '<button [disabled]="habilitarBotones" style="padding:3px;" class="btn btn-simple btn-info info btn-icon" ' +
        'matTooltip="Revisado" (click)="onClick(1)"><i class="material-icons">done</i></button>' +
        '<button [disabled]="habilitarBotones" style="padding:3px;" class="btn btn-simple btn-danger warning btn-icon" ' +
        'matTooltip="Rechazado" (click)="onClick(2)"><i class="material-icons">close</i></button>'+
        '<button [disabled]="habilitarBotones" style="padding:3px;" class="btn btn-simple btn-danger warning btn-icon" ' +
        'matTooltip="Dismiss" (click)="onClick(3)"><i class="material-icons">redo</i></button>'
})

export class ClientOptionsComponent implements ViewCell, OnInit {

    renderValue: string;
    public habilitarBotones: boolean = false;
    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        this.renderValue = this.value.toString().toUpperCase();
        if (this.rowData.Status !="Recibido"){
            this.habilitarBotones=true;
        }
    }

    onClick(type) {
        if (type == 1) {
            this.rowData.Type = "revisado";
        } else if (type == 2) {
            this.rowData.Type = "rechazado";
        }else{
            this.rowData.Type = "dismiss";
        }
        this.save.emit(this.rowData);
    }
}