import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';



@Component({
    selector: 'button-view',
    templateUrl: 'linkRutUrl.component.html'
  })

export class LinkRutUrlComponent implements ViewCell, OnInit {

    public imagenUrl: string;
    public labelUrl: string;
    renderValue: string;
    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {

        this.imagenUrl =  this.rowData.UrlImageRutPassport ;
        if(this.imagenUrl !=null)
        {
            this.labelUrl="Imagen Rut";
        }
        else
        {
            this.labelUrl = "";
        }
        if(this.value!=null)
        {
            this.renderValue = this.value.toString().toUpperCase();
        }

    }

    onClick() {
        this.save.emit(this.rowData);
    }
}