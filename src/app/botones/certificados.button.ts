import { Component, Input, OnInit, EventEmitter, Output  } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-view',
    templateUrl: 'certificados.button.html' 
})

export class CertificadoButtonComponent implements ViewCell, OnInit {
    
    renderValue: string;
    public habilitarBotones:boolean=false;
    public tieneCertificado: boolean = false;

    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        if(this.rowData.FileName){
            this.tieneCertificado=true;
        }
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        if(!curentUser.IsOwner)
            this.habilitarBotones=true;
        this.renderValue = this.value.toString().toUpperCase();
    }

    onClick(type) {
        if (type==1){
            this.rowData.Type ="info"; 
        }else if(type==2){
            this.rowData.Type = "edit";
        } else if (type == 3){
            this.rowData.Type = "eliminar";
        }
        else{
            this.rowData.Type = "descargar";
        }
        this.save.emit(this.rowData); 
    }
}