import { Component, Input, OnInit, EventEmitter, Output  } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-view',
    template: '<button style="padding:3px;" class="btn btn-simple btn-info info btn-icon" '+
    'matTooltip="Editar" (click)="onClick(1)"><i class="material-icons">edit</i></button>'+
    '<button style="padding:3px;" class="btn btn-simple btn-danger warning btn-icon" '+
    'matTooltip="Eliminar" (click)="onClick(2)"><i class="material-icons">close</i></button>'
})

export class EmpresasButtonComponent implements ViewCell, OnInit {
    
    renderValue: string;

    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        this.renderValue = this.value.toString().toUpperCase();
    }

    onClick(type) {
        if(type==1){
            this.rowData.Type = "edit";
        } else if (type == 2){
            this.rowData.Type = "eliminar";
        }
        this.save.emit(this.rowData); 
    }
}