import { Component, Input, OnInit, EventEmitter, Output  } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';

@Component({
    selector: 'button-view',
    template: '<button [disabled]="habilitarBotones" style="padding:3px;" class="btn btn-simple btn-info info btn-icon" '+
    'matTooltip="Editar" (click)="onClick(1)"><i class="material-icons">edit</i></button>'+
    '<button [disabled]="habilitarBotones" style="padding:3px;" class="btn btn-simple btn-danger warning btn-icon" '+
    'matTooltip="Eliminar" (click)="onClick(2)"><i class="material-icons">close</i></button>'
/*     '<button style="padding:3px;" class="btn btn-simple btn-info info btn-icon" '+
    'matTooltip="Evaluar" (click)="onClick(3[disabled]="habilitarBotones" )"><i class="material-icons">face</i></button>' */
})

export class EmbarqueButtonComponent implements ViewCell, OnInit {
    
    renderValue: string;
    public habilitarBotones: boolean = false;
    @Input() value: string | number;
    @Input() rowData: any;

    @Output() save: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (!curentUser.IsOwner)
            this.habilitarBotones = true;
        this.renderValue = this.value.toString().toUpperCase();
    }

    onClick(type) {
        if(type==1){
            this.rowData.Type = "edit";
        } else {
            this.rowData.Type = "eliminar";
        }
        this.save.emit(this.rowData); 
    }
}