import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { Router, ActivatedRoute } from '@angular/router';
declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [{
    path: '/formulario',
    title: 'Formulario',
    type: 'link',
    icontype: 'formulario'
},
/* {
    path: '/components',
    title: 'Components',
    type: 'sub',
    icontype: 'apps',
    collapse: 'components',
    children: [
        {path: 'buttons', title: 'Buttons', ab:'B'},
        {path: 'grid', title: 'Grid System', ab:'GS'},
        {path: 'panels', title: 'Panels', ab:'P'},
        {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
        {path: 'notifications', title: 'Notifications', ab:'N'},
        {path: 'icons', title: 'Icons', ab:'I'},
        {path: 'typography', title: 'Typography', ab:'T'}
    ]
}, 
 {
    path: '/forms',
    title: 'Forms',
    type: 'sub',
    icontype: 'content_paste',
    collapse: 'forms',
    children: [
        {path: 'regular', title: 'Regular Forms', ab:'RF'},
        {path: 'extended', title: 'Extended Forms', ab:'EF'},
        {path: 'validation', title: 'Validation Forms', ab:'VF'},
        {path: 'wizard', title: 'Wizard', ab:'W'}
    ]
}, 
{
    path: '/tables',
    title: 'Tables',
    type: 'sub',
    icontype: 'grid_on',
    collapse: 'tables',
    children: [
        {path: 'regular', title: 'Regular Tables', ab:'RT'},
        {path: 'extended', title: 'Extended Tables', ab:'ET'},
        {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
    ]
}, */
/* {
    path: '/certificados',
    title: 'Certificados',
    type: 'sub',
    icontype: 'content_paste',
    collapse: 'certificados',
    children: [
        { path: 'agregar-certificados', title: 'Agregar', ab: 'AC' },
        { path: 'listar-certificados', title: 'Listar', ab: 'LC' }
    ]
},
{
    path: '/dispositivos',
    title: 'Otros Dispositivos',
    type: 'sub',
    icontype: 'devices_other',
    collapse: 'dispositivos',
    children: [
        { path: 'agregar-dispositivos', title: 'Agregar', ab: 'AD' },
        { path: 'listar-dispositivos', title: 'Listar', ab: 'LD' }
    ]
}, */
/* {
    path: '/embarque',
    title: 'Control de Embarque',
    type: 'sub',
    icontype: 'directions_boat',
    collapse: 'embarque',
    children: [
        { path: 'agregar-embarque', title: 'Agregar', ab: 'AE' },
        { path: 'listar-embarque', title: 'Listar', ab: 'LE' }
    ]
},
{
    path: '/imei',
    title: 'IMEI',
    type: 'link',
    icontype: 'vpn_key'
}, */
/* {
    path: '/empresas',
    title: 'Clientes',
    type: 'sub',
    icontype: 'group',
    collapse: 'empresas',
    children: [
        { path: 'agregar-empresas', title: 'Agregar', ab: 'AE' },
        { path: 'listar-empresas', title: 'Listar', ab: 'LE' }
    ]
},
{
    path: '/usuarios',
    title: 'Usuarios',
    type: 'sub',
    icontype: 'fingerprint',
    collapse: 'usuarios',
    children: [
        { path: 'agregar-usuarios', title: 'Agregar', ab: 'AU' },
        { path: 'listar-usuarios', title: 'Listar', ab: 'LU' }
    ]
},
{
    path: '/equipos',
    title: 'Equipos',
    type: 'sub',
    icontype: 'phonelink_setup',
    collapse: 'equipos',
    children: [
        { path: 'agregar-equipo', title: 'Agregar', ab: 'AE' },
        { path: 'listar-equipos', title: 'Listar', ab: 'LE' }
    ]
},
{
    path: '/tipo-pruebas',
    title: 'Tipos de Pruebas',
    type: 'sub',
    icontype: 'build',
    collapse: 'tipo-pruebas',
    children: [
        { path: 'agregar-tipos-prueba', title: 'Agregar', ab: 'AT' },
        { path: 'listar-tipos-prueba', title: 'Listar', ab: 'LT' }
    ]
},
{
    path: '/homologacion',
    title: 'Homologaciones',
    type: 'sub',
    icontype: 'mobile_friendly',
    collapse: 'homologacion',
    children: [
        { path: 'agregar-homologacion', title: 'Agregar', ab: 'AH' },
        { path: 'listar-homologacion', title: 'Listar', ab: 'LH' }
    ]
},
 */
    /* {
        path: '/maps',
        title: 'Maps',
        type: 'sub',
        icontype: 'place',
        collapse: 'maps',
        children: [
            {path: 'google', title: 'Google Maps', ab:'GM'},
            {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
            {path: 'vector', title: 'Vector Map', ab:'VM'}
        ]
    }, 
     {    
        path: '/widgets',
        title: 'Widgets',
        type: 'link',
        icontype: 'widgets'

    }
    ,{
        path: '/charts',
        title: 'Charts',
        type: 'link',
        icontype: 'timeline'

    },{
        path: '/calendar',
        title: 'Calendar',
        type: 'link',
        icontype: 'date_range'
    },{
        path: '/pages',
        title: 'Pages',
        type: 'sub',
        icontype: 'image',
        collapse: 'pages',
        children: [
            {path: 'pricing', title: 'Pricing', ab:'P'},
            {path: 'timeline', title: 'Timeline Page', ab:'TP'},
            {path: 'login', title: 'Login Page', ab:'LP'},
            {path: 'register', title: 'Register Page', ab:'RP'},
            {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
            {path: 'user', title: 'User Page', ab:'UP'}
        ]
    }   */
];


@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    constructor(private router: Router) {

    }
    public menuItems: any[];
    public nombre = "";
    public ocultar=true;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 2, suppressScrollX: true });
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
    logout() {
        localStorage.clear();
        this.router.navigate(['pages/login']);
    }
}
