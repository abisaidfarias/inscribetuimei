import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Globals } from 'app/globals';


@Injectable()
export class FormularioService {

    constructor(private http: Http, private router: Router, private globals: Globals) { }
    //GET
    saveImages(formData: any, idRegisterImei: number) {

        return this.http.post(this.globals.urlpathApi 
            + 'imei/RegisterImeiImagen?imeiRegisterId=' 
            + idRegisterImei, formData)
            .map((response: Response) => {
                return response.json();
            });
    }
    registerImei(registerImei: any): Observable<boolean> {
        const headers = new Headers(
            {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json; charset=utf-8",
            });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.globals.urlpathApi + 'imei/RegisterImei', registerImei, options)
            .map((response: Response) => { return response.json() });
    }

   
    

}