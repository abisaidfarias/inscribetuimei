import { Component, OnInit } from '@angular/core';
import { TableData } from '../md/md-table/md-table.component';
import { Router } from '@angular/router';
import { FormularioService } from 'app/formulario/formulario.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { IfObservable } from 'rxjs/observable/IfObservable';
import { Constant } from 'app/constant';


declare const $: any;
class Registro {
    Sim: number; 
    Nacionality:number;
    Rut:string;
    Name:string;
    Email:string;
    Phone:string;
    CommercialModel:string;
    Brand:string;
    PrimerImei:string;
    SegundoImei:string;
    PhoneOrigin:string;
    Description:string;
}

@Component({
    selector: 'app-formulario',
    templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit {
    constructor(private router: Router, private constantes: Constant,
         private formBuilder: FormBuilder, private formularioService: FormularioService) { }
    type: FormGroup;
    simSelected: number;
    nacionalidadSelected: number;
    marcaSelected: string;
    origenSelected: string;
    rut:string;
    nombre: string;
    email: string;
    telefono: string;
    modeloComercial: string;
    public imei1:string;
    public imei2: string;
    isDual=false;
    isSelectedSim=false;
    isResidente =false;
    isNacionalidadSelected=false;
    registro:Registro;
    mostrarErrorImei2=false;
    mostrarErrorCatcha = false;
    catchaToken:string=null;
    description:string='';
    aceptaTerminos=false;
    removeNotification = false;
    fieldNacionalidad:string;
    fieldRutPasaporte: string;
    fieldNombre: string;
    fieldTelefono: string;
    fieldMarca: string;
    fieldModelo: string;
    fieldPais: string;
    fieldLugar: string;
    fieldImagenRut: string;
    fieldImagenImeiElectronico: string;
    fieldImagenImeiFisico: string;
    fieldImagenBoleta: string;
    fieldImagenTimbrado: string;
    fieldTerminos: string;
    fieldPrecios1: string;
    fieldPrecios2: string;
    fieldPrecios3: string;
    fieldPrecios4: string;
    fieldPrecios5: string;
    fieldPrecios6: string;
    fieldImagenAdjuntar: string;
    fieldImagenEliminar: string;
    fieldImagenCambiar: string;
    fieldTitulo: string;
    fieldDefaultLugar:string;
    mostrarSubmit=false;

    public URL_IMAGE_IMEI_ELECTRONIC = "../../assets/img/find_IMEI_iphone_animation_2.gif";
    public URL_IMAGE_IMEI_PHYSICAL = "../../assets/img/imei-fisico.jpg";
    public URL_IMAGE_RUT = "../../assets/img/cadula-de-identidad.jpg";
    public URL_IMAGE_PASAPORTE = "../../assets/img/pasaporte.jpg";
    public URL_IMAGE_PASSPORT_STAMPED = "../../assets/img/stamped.jpg";
    public URL_IMAGE_RECEIPT = "../../assets/img/receipt.jpg";

    public TITLE_IMEI_ELECTRONIC = "IMEI Electrónico";
    public TITLE_IMEI_PHYSICAL = "IMEI Fisico";
    public TITLE_RUT = "Rut o Pasaporte";
    public TITLE_PASAPORTE = "Rut o Pasaporte";
    public TITLE_PASSPORT_STAMPED = "Comprobante de Ingreso al pais";
    public TITLE_RECEIPT = "Boleta o factura de Compra";

    public TAG_RECEIPT ="De haberse adquirido el equipo presencialmente en el extranjero y no contar con los antecedentes anteriores, se solicita que se acredite la verosimilitud de dicha adquisición acompañando los antecedentes que comprueben la permanecía reciente en el extranjero con antigüedad no superior a 30 días, ya sea a través del pasaporte debidamente timbrado o pasajes emitidos por una empresa de transporte internacional a nombre del solicitante y sin perjuicio de otros antecedentes adicionales que puedan acompañarse, no se considera valido documentos emitidos por el solicitante. Ejemplos: cartas explicativas, capturas de pantalla, entre otros.";
    public TAG_IMEI_ELECTRONIC = "";
    public TAG_IMEI_PHYSICAL = "Habitualmente impreso en la parte posterior del equipo en la carcasa o al interior del teléfono o en el módulo para insertar la Simcard. (lo último es aplicable a los equipos Apple Iphone 7 en adelante), este consta de una numeración de 15 dígitos.";
    public TAG_RUT = "";
    public TAG_PASAPORTE = "";
    public TAG_PASSPORT_STAMPED = "Comprobante de ingreso al país emitido por una autoridad competente o pasaporte con timbre de ingreso, con antigüedad no superior a 2 meses, o pasajes de ida y vuelta o documento equivalente emitido por la empresa de transportes o agencia de viajes (cualquiera de ellos).";


    public MAX_IMAGE_SIZE = 1500000;

    sims = [
        { value: '0', viewValue: 'Simple' },
        { value: '1', viewValue: 'Dual' }
    ];
    nacionalidades = [
        { value: '0', viewValue: 'Chileno ó Residente' },
        { value: '1', viewValue: 'Extranjero' }
    ];
    marcas=[
        { value: 'NO SELECCIONADA', viewValue: 'NO SELECCIONADA' },
        { value: 'ACCURA', viewValue: 'ACCURA' },
        { value: 'ACER', viewValue: 'ACER' },
        { value: 'ALCATEL', viewValue: 'ALCATEL' },
        { value: 'AMPER', viewValue: 'AMPER' },
        { value: 'APPLE', viewValue: 'APPLE' },
        { value: 'ASKEY', viewValue: 'ASKEY' },
        { value: 'ASUS', viewValue: 'ASUS' },
        { value: 'AZUMI', viewValue: 'AZUMI' },
        { value: 'AMAZING', viewValue: 'AMAZING' },
        { value: 'BLACKBERRY', viewValue: 'BLACKBERRY' },
        { value: 'BLU', viewValue: 'BLU' },
        { value: 'BLUBOO', viewValue: 'BLUBOO' },
        { value: 'BLUE', viewValue: 'BLUE' },
        { value: 'BLUE-CASTLE', viewValue: 'BLUE-CASTLE' },
        { value: 'BLUEBIRD', viewValue: 'BLUEBIRD' },
        { value: 'BMOBILE', viewValue: 'BMOBILE' },
        { value: 'BLACKVIEW', viewValue: 'BLACKVIEW' },
        { value: 'CALAMP', viewValue: 'CALAMP' },
        { value: 'CINTERION', viewValue: 'CINTERION' },
        { value: 'COBAN', viewValue: 'COBAN' },
        { value: 'CONCOX', viewValue: 'CONCOX' },
        { value: 'DAHUA', viewValue: 'DAHUA' },
        { value: 'DATALOGIC', viewValue: 'DATALOGIC' },
        { value: 'DBLUE', viewValue: 'DBLUE' },
        { value: 'DCT', viewValue: 'DCT' },
        { value: 'DIQIUTUXING', viewValue: 'DIQIUTUXING' },
        { value: 'DOOGEE', viewValue: 'DOOGEE' },
        { value: 'DOTINCORP', viewValue: 'DOTINCORP' },
        { value: 'DSC', viewValue: 'DSC' },
        { value: 'EKS', viewValue: 'EKS' },
        { value: 'ESSENCE SMARTCARE', viewValue: 'ESSENCE SMARTCARE' },
        { value: 'EVIEW INDUSTRIAL LIMITED', viewValue: 'EVIEW INDUSTRIAL LIMITED' },
        { value: 'EXEMYS', viewValue: 'EXEMYS' },
        { value: 'FALTCOM', viewValue: 'FALTCOM' },
        { value: 'FIBOCOM', viewValue: 'FIBOCOM' },
        { value: 'GEAR', viewValue: 'GEAR' },
        { value: 'GEOTAB', viewValue: 'GEOTAB' },
        { value: 'GPS TRACKER', viewValue: 'GPS TRACKER' },
        { value: 'GRETEL', viewValue: 'GRETEL' },
        { value: 'GEMALTO', viewValue: 'GEMALTO' },
        { value: 'GENERICO', viewValue: 'GENERICO' },
        { value: 'GREEN E', viewValue: 'GREEN E' },
        { value: 'HISENSE', viewValue: 'HISENSE' },
        { value: 'HTC', viewValue: 'HTC' },
        { value: 'HUAWEI', viewValue: 'HUAWEI' },
        { value: 'HONEYWELL', viewValue: 'HONEYWELL' },
        { value: 'I-TRAC', viewValue: 'I-TRAC' },
        { value: 'IDISPLAY', viewValue: 'IDISPLAY' },
        { value: 'INGENICO', viewValue: 'INGENICO' },
        { value: 'ITECOM', viewValue: 'ITECOM' },
        { value: 'JIMI', viewValue: 'JIMI' },
        { value: 'JOINTECH', viewValue: 'JOINTECH' },
        { value: 'KEIPHONE', viewValue: 'KEIPHONE' },
        { value: 'KINGNEED', viewValue: 'KINGNEED' },
        { value: 'LANIX', viewValue: 'LANIX' },
        { value: 'LAVA', viewValue: 'LAVA' },
        { value: 'LENFO', viewValue: 'LENFO' },
        { value: 'LENOVO', viewValue: 'LENOVO' },
        { value: 'LG', viewValue: 'LG' },
        { value: 'LACROIX-SOFREL', viewValue: 'LACROIX-SOFREL' },
        { value: 'LEECO', viewValue: 'LEECO' },
        { value: 'MASTERGME-G', viewValue: 'MASTERGME-G' },
        { value: 'MEG TECH LLC', viewValue: 'MEG TECH LLC' },
        { value: 'MEIZU', viewValue: 'MEIZU' },
        { value: 'MICRONET', viewValue: 'MICRONET' },
        { value: 'MICROSOFT', viewValue: 'MICROSOFT' },
        { value: 'MOMO', viewValue: 'MOMO' },
        { value: 'MONTSENY BALAST', viewValue: 'MONTSENY BALAST' },
        { value: 'MOTOROLA', viewValue: 'MOTOROLA' },
        { value: 'MOVILTELCO', viewValue: 'MOVILTELCO' },
        { value: 'MULTITECH', viewValue: 'MULTITECH' },
        { value: 'MIXTELEMATIX', viewValue: 'MIXTELEMATIX' },
        { value: 'MOBILE SAFETY', viewValue: 'MOBILE SAFETY' },
        { value: 'NEXUS', viewValue: 'NEXUS' },
        { value: 'NIMBELINK', viewValue: 'NIMBELINK' },
        { value: 'NOKIA', viewValue: 'NOKIA' },
        { value: 'NOVATEL WIRELESS', viewValue: 'NOVATEL WIRELESS' },
        { value: 'OBSERVA', viewValue: 'OBSERVA' },
        { value: 'ONE PLUS', viewValue: 'ONE PLUS' },
        { value: 'OPPO', viewValue: 'OPPO' },
        { value: 'ORBCOMM', viewValue: 'ORBCOMM' },
        { value: 'OWN', viewValue: 'OWN' },
        { value: 'OPTICON', viewValue: 'OPTICON' },
        { value: 'PETALWAYS', viewValue: 'PETALWAYS' },
        { value: 'PROSAT', viewValue: 'PROSAT' },
        { value: 'PARADOX', viewValue: 'PARADOX' },
        { value: 'QECLINK', viewValue: 'QECLINK' },
        { value: 'QUALCOMM', viewValue: 'QUALCOMM' },
        { value: 'QUECTEL', viewValue: 'QUECTEL' },
        { value: 'QUANTUM', viewValue: 'QUANTUM' },
        { value: 'QUECLINK', viewValue: 'QUECLINK' },
        { value: 'ROBUSTEL', viewValue: 'ROBUSTEL' },
        { value: 'RUPTELA', viewValue: 'RUPTELA' },
        { value: 'SAMSUNG', viewValue: 'SAMSUNG' },
        { value: 'SAVETREK', viewValue: 'SAVETREK' },
        { value: 'SECAR', viewValue: 'SECAR' },
        { value: 'SEEING MACHINES', viewValue: 'SEEING MACHINES' },
        { value: 'SEGTV', viewValue: 'SEGTV' },
        { value: 'SENTER', viewValue: 'SENTER' },
        { value: 'SHANGHAI SIMCOM', viewValue: 'SHANGHAI SIMCOM' },
        { value: 'SIMCOM', viewValue: 'SIMCOM' },
        { value: 'SIMPLIFI', viewValue: 'SIMPLIFI' },
        { value: 'SINO', viewValue: 'SINO' },
        { value: 'SINO TRACK', viewValue: 'SINO TRACK' },
        { value: 'SKY DEVICES', viewValue: 'SKY DEVICES' },
        { value: 'SONY', viewValue: 'SONY' },
        { value: 'SPECTRA TECHNOLOGIES', viewValue: 'SPECTRA TECHNOLOGIES' },
        { value: 'SHENZEN XYD TECHNOLOGIES', viewValue: 'SHENZEN XYD TECHNOLOGIES' },
        { value: 'SIEMENS', viewValue: 'SIEMENS' },
        { value: 'SIERRA WIRELESS', viewValue: 'SIERRA WIRELESS' },
        { value: 'SINOPRO', viewValue: 'SINOPRO' },
        { value: 'SUNTECH', viewValue: 'SUNTECH' },
        { value: 'TAPING', viewValue: 'TAPING' },
        { value: 'TELIT', viewValue: 'TELIT' },
        { value: 'TELTONIKA', viewValue: 'TELTONIKA' },
        { value: 'TOP ADAPTADOR', viewValue: 'TOP ADAPTADOR' },
        { value: 'TOPICON', viewValue: 'TOPICON' },
        { value: 'TELPO', viewValue: 'TELPO' },
        { value: 'U-BLOX AG', viewValue: 'U-BLOX AG' },
        { value: 'UBLOX', viewValue: 'UBLOX' },
        { value: 'UNITECH', viewValue: 'UNITECH' },
        { value: 'VERIFONE', viewValue: 'VERIFONE' },
        { value: 'VERYKOOL', viewValue: 'VERYKOOL' },
        { value: 'VWAR', viewValue: 'VWAR' },
        { value: 'WAC', viewValue: 'WAC' },
        { value: 'WEIFU', viewValue: 'WEIFU' },
        { value: 'WEPOY', viewValue: 'WEPOY' },
        { value: 'WNC', viewValue: 'WNC' },
        { value: 'WIKO', viewValue: 'WIKO' },
        { value: 'XIAOMI', viewValue: 'XIAOMI' },
        { value: 'XIRGO', viewValue: 'XIRGO' },
        { value: 'YEZZ', viewValue: 'YEZZ' },
        { value: 'ZEBRA', viewValue: 'ZEBRA' },
        { value: 'ZNONE', viewValue: 'ZNONE' },
        { value: 'ZTE', viewValue: 'ZTE' },
        { value: 'OTRAS MARCAS', viewValue: 'OTRAS MARCAS' }  
    ];
    myFiles: string[] = [];
    nameFiles: string[] = [];

    isFieldValid(form: FormGroup, field: string) {
        return !form.get(field).valid && form.get(field).touched;
    }

    displayFieldCss(form: FormGroup, field: string,force:boolean) {
        
        return {
            'has-error': this.isFieldValid(form, field),
            'has-feedback': this.isFieldValid(form, field)
        };
    }
    verificarMensajes() {

        if (this.imei2 == undefined || this.imei2.length != 15) {
            this.mostrarErrorImei2 = true;
        } else {
            this.mostrarErrorImei2 = false;
        }
        if (!this.catchaToken) {
            this.mostrarErrorCatcha = true;
        }
        else {
            this.mostrarErrorCatcha = false;
        }
    }
    enableButton(){
        if (this.catchaToken && this.aceptaTerminos) {
            alert('ok');
        }
    }
    onSubmit() {
        if(!this.isResidente){
            this.type.get('receipt').clearValidators();
            this.type.get('receipt').updateValueAndValidity();  
        }
        else{
            this.type.get('stampedPassport').clearValidators();
            this.type.get('stampedPassport').updateValueAndValidity(); 
        }
        if (this.type.valid && this.aceptaTerminos) {
            let registro = new Registro();
            registro.Sim = this.simSelected;
            registro.Phone = this.telefono;
            registro.Rut = this.rut;
            registro.Name = this.nombre;
            registro.Nacionality = this.nacionalidadSelected;
            registro.CommercialModel = this.modeloComercial;
            registro.Email = this.email;
            registro.Brand = this.marcaSelected;
            registro.PrimerImei = this.imei1;
            registro.SegundoImei = this.imei2;
            registro.PhoneOrigin = this.origenSelected;
            registro.Description = this.description;
            if (!this.catchaToken) {
                this.popUpErrorCamposRequeridos();
                this.verificarMensajes();
                return;
            }
            if(registro.Sim==1)
            {
                if(!registro.SegundoImei)
                {
                    this.popUpErrorCamposRequeridos();
                    this.verificarMensajes();
                    return;
                }
            }
            this.registerImei(registro);
        } else {
            this.popUpErrorCamposRequeridos();
            this.validateAllFormFields(this.type);
            this.verificarMensajes();
        } 
    }
    popUpErrorCamposRequeridos(){
        swal({
            type: 'error',
            html: '<strong>Verifique los campos requeridos</strong>',
            confirmButtonClass: 'btn btn-danger',
            buttonsStyling: false
        })
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
    ngOnInit() {
        this.traducirEspanol();
        this.type = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            nombre: [null, Validators.required],
            rut: [null, Validators.required],
            email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            telefono: [null, [Validators.required, Validators.minLength(9), Validators.maxLength(9)]],
            marca: [null, Validators.required],
            modelo: [null, Validators.required],
            sim: [null, Validators.required],
            nacionalidad: [null, Validators.required],
            imei1: [null, [Validators.required, Validators.minLength(15), Validators.maxLength(15)]],
            rutImage: [null, Validators.required],
            receipt: [null, Validators.required],
            stampedPassport: [null, Validators.required],
            electronicIMEI: [null, Validators.required],
            imei2: [null, [Validators.minLength(15), Validators.maxLength(15)]],
            terminos: [null, ''],
            origen: [null, Validators.required],
            comment: [null, Validators.maxLength(200)]
                      
        })
    }
    tipoSim(){
        this.isSelectedSim=true;
        if(this.simSelected==1){
            this.isDual=true;
        }
        else
        {
            this.isDual=false;
        }
    }
    tipoNacionalidad() {
        this.isNacionalidadSelected=true;
        if (this.nacionalidadSelected == 0) {
            this.isResidente = true;
        }
        else {
            this.isResidente = false;
        }

    }
    uploadFiles(idRegisterImei:number) {
        const frmData = new FormData();
        for (var i = 0; i < this.myFiles.length; i++) {
            frmData.append(this.nameFiles[i], this.myFiles[i],this.nameFiles[i]);
        }
        this.saveImages(frmData,idRegisterImei)
    }
    saveImages(frmData: FormData, idRegisterImei:number) {
        this.formularioService.saveImages(frmData,idRegisterImei)
            .subscribe(
                (servers: any) => {
                    swal.close();
                    swal({
                        type: 'success',
                        html: '<strong>Su solicitud ha sido recibida con número de solicitud:'+idRegisterImei+'</strong>',
                        confirmButtonClass: 'btn btn-success',
                        buttonsStyling: false,
                        allowOutsideClick: false
                    })
                    .then(() => {
                        window.location.reload();
                    });
                },
                (error) => {                   
                    if (error._body)
                    {
                        let mensajeError = error.json();
                        swal.close();
                        swal({
                            type: 'error',
                            html: '<strong>' + mensajeError.Message+'</strong>',
                            confirmButtonClass: 'btn btn-danger',
                            buttonsStyling: false
                        });
                    }else{
                        swal.close();
                        swal({
                            type: 'error',
                            html: '<strong>Error en la Operación</strong>',
                            confirmButtonClass: 'btn btn-danger',
                            buttonsStyling: false
                        });
                    }
                    
                    
                }
            );
    }
    registerImei(register) {
        swal({
            title: 'Enviando Solicitud',
            allowOutsideClick: false
        });
        swal.showLoading();
        this.formularioService.registerImei(register)
        .subscribe(
            (registerImeId: any) => {
                this.uploadFiles(registerImeId);
            },
            (error) => {
                swal.close();
                swal({
                    type: 'error',
                    html: '<strong>Error en la Operación</strong>',
                    confirmButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                });
            }
        );
    }
    popUpInfoImage(type){
        let urlImage =this.getUrlImage(type);
        let title = this.getTitleImage(type);
        let tag = this.getTag(type);
        swal({
            title: "",
            html: '<div class="form-group">' +
                '<div class="row">' +
                '<h4><strong>' + title +'</strong></h4>'+
                '</div>' +
                '<div class="row">' +
                '<img style="width:300px;" src="'+urlImage+'">'+
                '</div>'+
                '<div class="row">' +
                '<label style="margin: 20px;text-align:justify" >' + tag + '</label>'+
                '</div>' +
                '</div>',
            showCancelButton: false,
            confirmButtonClass: 'btn btn-success',
            buttonsStyling: false,
            width: "500px"
        })
    }
    getUrlImage(type){
        if (type =="electronicIMEI")
        {
            return this.URL_IMAGE_IMEI_ELECTRONIC;
        }
        if (type == "physicalIMEI") {
            return this.URL_IMAGE_IMEI_PHYSICAL;
        }
        if (type == "rutImage") {
            if (this.isResidente){
                return this.URL_IMAGE_RUT;
            }
            else{
                return this.URL_IMAGE_PASAPORTE;
            }
        }
        if (type == "receipt") {
            return this.URL_IMAGE_RECEIPT;
        }
        if (type == "stampedPassport") {
            return this.URL_IMAGE_PASSPORT_STAMPED;
        }
    }
    getTitleImage(type){
        if (type == "electronicIMEI") {
            return this.TITLE_IMEI_ELECTRONIC;
        }
        if (type == "physicalIMEI") {
            return this.TITLE_IMEI_PHYSICAL;
        }
        if (type == "rutImage") {
            if (this.isResidente) {
                return this.TITLE_RUT;
            }
            else {
                return this.TITLE_PASAPORTE;
            }
        }
        if (type == "receipt") {
            return this.TITLE_RECEIPT;
        }
        if (type == "stampedPassport") {
            return this.TITLE_PASSPORT_STAMPED;
        }
    }
    getTag(type) {
        if (type == "electronicIMEI") {
            return this.TAG_IMEI_ELECTRONIC;
        }
        if (type == "physicalIMEI") {
            return this.TAG_IMEI_PHYSICAL;
        }
        if (type == "rutImage") {
            if (this.isResidente) {
                return this.TAG_RUT;
            }
            else {
                return this.TAG_PASAPORTE;
            }
        }
        if (type == "receipt") {
            return this.TAG_RECEIPT;
        }
        if (type == "stampedPassport") {
            return this.TAG_PASSPORT_STAMPED;
        }
    }
    resolved(captchaResponse: string) {      
        this.catchaToken = captchaResponse;
        this.verificarMensajes();
        if(this.catchaToken && this.aceptaTerminos)
        {
            this.mostrarSubmit=true;
        }else{
            this.mostrarSubmit=false;
        }
    }
    mostrarTerminos() {
        swal({
            title: "",
            html: '<div class="form-group">' +
                '<h3>Términos y Condiciones.</h3>' +
                '<ul>' +
                '<li>' +
                '<div style="font-size:16px;text-align:justify">Artículo 9° En el caso de los terminales que sean ingresados al país por personas naturales para su uso personal, aquéllas deberán, al momento de su adquisición en el extranjero, verificar las características técnicas del equipo en relación a su compatibilidad con el SAE. Sin perjuicio de lo anterior, y para efectos de la presente norma, se presumirá la no compatibilidad de tales equipos con el SAE, debiendo los propios usuarios asumir las consecuencias derivadas de dicha situación. Las empresas certificadoras, con motivo de la Inscripción Administrativa a que se refiere la resolución de esta Subsecretaría que establece las especificaciones técnicas mínimas que deben cumplir los equipos terminales utilizados en las redes móviles, dejarán constancia de esta circunstancia al momento del ingreso del equipo en la base de datos centralizada ahí señalada, precisando que se realizó el procedimiento administrativo y que el equipo no es apto SAE.</div>' +
                '<div style="border-radius: 6px;border: 2px solid #F44336;padding: 20px;font-size: 20px;font-weight: 600;"><p align ="justify">EN RESUMEN NO ES POSIBLE ASEGURAR QUE ESTE TELÉFONO:</p><p align = "justify"> -Reciba el mensaje de alerta de emergencia emitido por ONEMI.</p><p align = "justify" > -Funcione en todas las empresas móviles del país.</p></div>'+
                '<div style="font-size:16px;text-align:justify">&nbsp;</div>' +
                '<div style="font-size:16px;text-align:justify">RES-1463 EXENTA_16-JUN-2016 (VERSIÓN 30-06-2017)</div>' +
                '<div style="font-size:16px;text-align:justify">Artículo 8º. En el caso de los terminales que sean ingresados al país por personas naturales para su uso personal, será responsabilidad de estas últimas, al momento de su adquisición, la verificación de las características técnicas del equipo en relación al cumplimiento del soporte de bandas señaladas en el Anexo I. El soporte de las bandas del terminal deberá acreditarse documentalmente ante alguna de las empresas certificadoras, las que, a su vez, deberán realizar una validación Documental de los datos del equipo. Cumplido lo anterior, será la misma empresa certificadora la que deberá incorporar a la base de datos centralizada, los datos correspondientes a dicho terminal, precisando que se trata de una Inscripción Administrativa, de conformidad a lo Previsto en la letra k) del artículo 3º de la presente resolución. La validación documental e Inscripción Administrativa del equipo no irrogará costo alguno a los usuarios requirentes de ella, siempre y cuando aquélla no supere la cantidad de un equipo terminal al año por persona natural. Con todo, cada empresa certificadora sólo estará obligada a esta validación documental e Inscripción Administrativa de equipos, en forma gratuita, con un límite máximo anual de un 2% del total anual de los ingresos a la base de datos centralizada por empresa certificadora. Para efectos de este artículo y de lo previsto en la Ley N° 19.628, Sobre Protección de la Vida Privada, la empresa certificadora deberá recabar la autorización por escrito de la persona natural que requiere la validación e Inscripción Administrativa de su equipo, en lo concerniente al almacenamiento y tratamiento de aquellos datos que sean estrictamente necesarios para llevar a cabo dicho procedimiento y la aplicación de la presente norma. La autorización anterior deberá constar de manera expresa en la solicitud, formulario o requerimiento de este tipo de homologación, conjuntamente con la identificación de la persona natural correspondiente, debiendo comprobarse siempre en el respectivo proceso, sea éste presencial o remoto, la correspondencia entre el IMEI impreso en el equipo y el IMEI obtenido por configuración, la circunstancia de haberse adquirido el equipo en el extranjero o, a lo menos, la estadía de su titular en el Extranjero y las características del equipo en base al respectivo manual o ficha técnica en que conste el soporte de las bandas de frecuencia. Cada solicitud Inscripción Administrativa podrá referirse a un solo equipo terminal. Por su parte, para proceder a la inscripción en la base de datos de los equipos señalados en el artículo anterior, las empresas certificadoras también deberán recabar la autorización indicada precedentemente en caso que el solicitante sea una persona natural. Asimismo, y junto con la identificación de la persona natural o jurídica que solicita la inscripción y la fecha de esta última, deberá identificarse el tipo de dispositivo, la marca, el modelo técnico, el IMEI y las respectivas facturas, órdenes de compra u otros documentos que den cuenta de su adquisición, internamiento o fabricación. Este proceso podrá también ser presencial o remoto.</div>' +
                '</li>' +
                '</ul>' +
                '<ul>' +
                '<li>' +
                '<a style="font-size:16px;text-align:justify" href="https://docs.wixstatic.com/ugd/0adc07_15e5f00105cd429f84e1a00177c34e9a.pdf" target="_blank"></a>' +
                '<strong>Adjuntas Resoluciones exentas Bandas y SAE:</strong>&nbsp; &nbsp;&nbsp;' +
                '<a style="font-size:16px;text-align:justify" href="https://docs.wixstatic.com/ugd/0adc07_15e5f00105cd429f84e1a00177c34e9a.pdf" target="_blank">RES 1474</a>&nbsp; &nbsp; &nbsp; &nbsp;' +
                '<a style="font-size:16px;text-align:justify" href="https://docs.wixstatic.com/ugd/0adc07_8091a20e83b8480ea5b007552d06eedc.pdf" target="_blank">RES 1463</a>' +
                '</li>' +
                '</ul>' +
                '<p style="font-size:16px;text-align:justify"><br>Sin prejuicio de lo anterior, esta normativa aplica en silencio, dandose primera prioridad a:&nbsp;<br><strong><br>Constitución Política de la República 1980, Capítulo III "De los Derechos y Deberes Constitucionales" artículo 19</strong></p>' +
                '<p style="font-size:16px;text-align:justify">22º.- La no discriminación arbitraria en el trato que deben dar el Estado y sus&nbsp;organismos en materia económica.&nbsp;Sólo en virtud de una ley, y siempre que no signifique tal discriminación, se&nbsp;podrán autorizar determinados beneficios directos o indirectos en favor de algún&nbsp;sector, actividad o zona geográfica, o establecer gravámenes especiales que afecten a&nbsp;uno u otras. En el caso de las franquicias o beneficios indirectos, la estimación del&nbsp;costo de éstos deberá incluirse anualmente en la Ley de Presupuestos;</p>' +
                '<p style="font-size:16px;text-align:justify">23º.- La libertad para adquirir el dominio de toda clase de bienes, excepto&nbsp;aquellos que la naturaleza ha hecho comunes a todos los hombres o que deban&nbsp;pertenecer a la Nación toda y la ley lo declare así. Lo anterior es sin perjuicio de lo&nbsp;prescrito en otros preceptos de esta Constitución.&nbsp;Una ley de quórum calificado y cuando así lo exija el interés nacional puedeestablecer limitaciones o requisitos para la adquisición del dominio de algunos bienes;</p>' +
                '<p style="font-size:16px;text-align:justify">24°.-&nbsp;"El Derecho de Propiedad en sus diversas especies sobre toda clase de bienes corporales o incorporales."</p>' +
                '</div>',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger', 
            buttonsStyling: false,
            width: "850px"
        })
        if(this.catchaToken && this.aceptaTerminos)
        {
            this.mostrarSubmit=true;
        }
        else{
            this.mostrarSubmit=false;
        }
    }
    eliminarImagen(nameFile){

        for (var i = 0; i < this.nameFiles.length; i++) {
            if (this.nameFiles[i] == nameFile)
            {
                this.myFiles.splice(i,1);
                this.nameFiles.splice(i,1);
            }
        }
    }
    addFile(file, nameFile,formId) {
        if (file.target.files[0].size > this.MAX_IMAGE_SIZE) {
            swal({
                type: 'error',
                html: '<strong>Imagen excede tamaño máximo permitido(2mb)</strong>',
                confirmButtonClass: 'btn btn-danger',
                buttonsStyling: false
            });
            $("#" + file.target.id).val('');
            return;
        }
        this.eliminarImagen(nameFile);      
        for (var i = 0; i < file.target.files.length; i++) {
            this.myFiles.push(file.target.files[i]);
            this.nameFiles.push(nameFile);
        }
    }
    remove(){
        this.removeNotification=true;
    }
    traducirEspanol(){
        this.fieldRutPasaporte= this.constantes.RutoPasaporteEs;
        this.fieldNacionalidad=this.constantes.NacionalidadEs;
        this.fieldTerminos=this.constantes.AcuedoServicioEs;
        this.fieldTelefono = this.constantes.TelefonoEs;
        this.fieldPais = this.constantes.PaisOrigenEs;
        this.fieldNombre = this.constantes.NombreEs;
        this.fieldModelo = this.constantes.ModelEs;
        this.fieldMarca = this.constantes.MarcaEs;
        this.fieldLugar = this.constantes.LugarEs;
        this.fieldImagenTimbrado = this.constantes.ImageTimbradoEs;
        this.fieldImagenRut = this.constantes.ImageRutPasaporteEs;
        this.fieldImagenImeiFisico = this.constantes.ImageImeiFisicoEs;
        this.fieldImagenImeiElectronico = this.constantes.ImageImeiElectronicoEs;
        this.fieldImagenBoleta = this.constantes.ImageFacturaEs;
        this.fieldPrecios1=this.constantes.CostoServicio1Es;
        this.fieldPrecios2 = this.constantes.CostoServicio2Es;
        this.fieldPrecios3 = this.constantes.CostoServicio3Es;
        this.fieldPrecios4 = this.constantes.CostoServicio4Es;
        this.fieldPrecios5 = this.constantes.CostoServicio5Es;
        this.fieldPrecios6 = this.constantes.CostoServicio6Es;
        this.fieldImagenAdjuntar = this.constantes.ImagenAdjuntarEs;
        this.fieldImagenEliminar = this.constantes.ImagenEliminarEs;
        this.fieldImagenCambiar = this.constantes.ImagenCambiarEs;
        this.fieldTitulo=this.constantes.TituloEs;
        this.fieldDefaultLugar = this.constantes.LugarDescripcionEs;
        
    } 
    traducirIngles() {
        this.fieldRutPasaporte = this.constantes.RutoPasaporteEn;
        this.fieldNacionalidad = this.constantes.NacionalidadEn;
        this.fieldTerminos = this.constantes.AcuedoServicioEn;
        this.fieldTelefono = this.constantes.TelefonoEn;
        this.fieldPais = this.constantes.PaisOrigenEn;
        this.fieldNombre = this.constantes.NombreEn;
        this.fieldModelo = this.constantes.ModelEn;
        this.fieldMarca = this.constantes.MarcaEn;
        this.fieldLugar = this.constantes.LugarEn;
        this.fieldImagenTimbrado = this.constantes.ImageTimbradoEn;
        this.fieldImagenRut = this.constantes.ImageRutPasaporteEn;
        this.fieldImagenImeiFisico = this.constantes.ImageImeiFisicoEn;
        this.fieldImagenImeiElectronico = this.constantes.ImageImeiElectronicoEn;
        this.fieldImagenBoleta = this.constantes.ImageFacturaEn;
        this.fieldPrecios1 = this.constantes.CostoServicio1En;
        this.fieldPrecios2 = this.constantes.CostoServicio2En;
        this.fieldPrecios3 = this.constantes.CostoServicio3En;
        this.fieldPrecios4 = this.constantes.CostoServicio4En;
        this.fieldPrecios5 = this.constantes.CostoServicio5En;
        this.fieldPrecios6 = this.constantes.CostoServicio6En;
        this.fieldImagenAdjuntar=this.constantes.ImagenAdjuntarEn;
        this.fieldImagenEliminar = this.constantes.ImagenEliminarEn;
        this.fieldImagenCambiar = this.constantes.ImagenCambiarEn;
        this.fieldTitulo = this.constantes.TituloEn;
        this.fieldDefaultLugar = this.constantes.LugarDescripcionEn;
    }
    paises = [
        { value: 'EEUU', viewValue: 'Estados Unidos' },
        { value: 'China', viewValue: 'China' },
        { value: 'Afganistán', viewValue: 'Afganistán' },
        { value: 'Albania', viewValue: 'Albania' },
        { value: 'Alemania', viewValue: 'Alemania' },
        { value: 'Andorra', viewValue: 'Andorra' },
        { value: 'Angola', viewValue: 'Angola' },
        { value: 'Anguilla', viewValue: 'Anguilla' },
        { value: 'Antártida', viewValue: 'Antártida' },
        { value: 'Antigua y Barbuda', viewValue: 'Antigua y Barbuda' },
        { value: 'Antillas Holandesas', viewValue: 'Antillas Holandesas' },
        { value: 'Arabia Saudí', viewValue: 'Arabia Saudí' },
        { value: 'Argelia', viewValue: 'Argelia' },
        { value: 'Argentina', viewValue: 'Argentina' },
        { value: 'Armenia', viewValue: 'Armenia' },
        { value: 'Aruba', viewValue: 'Aruba' },
        { value: 'ARY Macedonia', viewValue: 'ARY Macedonia' },
        { value: 'Australia', viewValue: 'Australia' },
        { value: 'Austria', viewValue: 'Austria' },
        { value: 'Azerbaiyán', viewValue: 'Azerbaiyán' },
        { value: 'Bahamas', viewValue: 'Bahamas' },
        { value: 'Bahréin', viewValue: 'Bahréin' },
        { value: 'Bangladesh', viewValue: 'Bangladesh' },
        { value: 'Barbados', viewValue: 'Barbados' },
        { value: 'Bélgica', viewValue: 'Bélgica' },
        { value: 'Belice', viewValue: 'Belice' },
        { value: 'Benin', viewValue: 'Benin' },
        { value: 'Bermudas', viewValue: 'Bermudas' },
        { value: 'Bhután', viewValue: 'Bhután' },
        { value: 'Bielorrusia', viewValue: 'Bielorrusia' },
        { value: 'Bolivia', viewValue: 'Bolivia' },
        { value: 'Bosnia y Herzegovina', viewValue: 'Bosnia y Herzegovina' },
        { value: 'Botsuana', viewValue: 'Botsuana' },
        { value: 'Brasil', viewValue: 'Brasil' },
        { value: 'Brunéi', viewValue: 'Brunéi' },
        { value: 'Bulgaria', viewValue: 'Bulgaria' },
        { value: 'Burkina Faso', viewValue: 'Burkina Faso' },
        { value: 'Burundi', viewValue: 'Burundi' },
        { value: 'Cabo Verde', viewValue: 'Cabo Verde' },
        { value: 'Camboya', viewValue: 'Camboya' },
        { value: 'Camerún', viewValue: 'Camerún' },
        { value: 'Canadá', viewValue: 'Canadá' },
        { value: 'Chad', viewValue: 'Chad' },
        { value: 'Chipre', viewValue: 'Chipre' },
        { value: 'Ciudad del Vaticano', viewValue: 'Ciudad del Vaticano' },
        { value: 'Colombia', viewValue: 'Colombia' },
        { value: 'Comoras', viewValue: 'Comoras' },
        { value: 'Congo', viewValue: 'Congo' },
        { value: 'Corea del Norte', viewValue: 'Corea del Norte' },
        { value: 'Corea del Sur', viewValue: 'Corea del Sur' },
        { value: 'Costa de Marfil', viewValue: 'Costa de Marfil' },
        { value: 'Costa Rica', viewValue: 'Costa Rica' },
        { value: 'Croacia', viewValue: 'Croacia' },
        { value: 'Cuba', viewValue: 'Cuba' },
        { value: 'Dinamarca', viewValue: 'Dinamarca' },
        { value: 'Dominica', viewValue: 'Dominica' },
        { value: 'Ecuador', viewValue: 'Ecuador' },
        { value: 'Egipto', viewValue: 'Egipto' },
        { value: 'El Salvador', viewValue: 'El Salvador' },
        { value: 'Emiratos Árabes Unidos', viewValue: 'Emiratos Árabes Unidos' },
        { value: 'Eritrea', viewValue: 'Eritrea' },
        { value: 'Eslovaquia', viewValue: 'Eslovaquia' },
        { value: 'Eslovenia', viewValue: 'Eslovenia' },
        { value: 'España', viewValue: 'España' },
        { value: 'Estonia', viewValue: 'Estonia' },
        { value: 'Etiopía', viewValue: 'Etiopía' },
        { value: 'Filipinas', viewValue: 'Filipinas' },
        { value: 'Finlandia', viewValue: 'Finlandia' },
        { value: 'Fiyi', viewValue: 'Fiyi' },
        { value: 'Francia', viewValue: 'Francia' },
        { value: 'Gabón', viewValue: 'Gabón' },
        { value: 'Gambia', viewValue: 'Gambia' },
        { value: 'Georgia', viewValue: 'Georgia' },
        { value: 'Ghana', viewValue: 'Ghana' },
        { value: 'Gibraltar', viewValue: 'Gibraltar' },
        { value: 'Granada', viewValue: 'Granada' },
        { value: 'Grecia', viewValue: 'Grecia' },
        { value: 'Groenlandia', viewValue: 'Groenlandia' },
        { value: 'Guadalupe', viewValue: 'Guadalupe' },
        { value: 'Guam', viewValue: 'Guam' },
        { value: 'Guatemala', viewValue: 'Guatemala' },
        { value: 'Guayana Francesa', viewValue: 'Guayana Francesa' },
        { value: 'Guinea', viewValue: 'Guinea' },
        { value: 'Guinea Ecuatorial', viewValue: 'Guinea Ecuatorial' },
        { value: 'Guinea-Bissau', viewValue: 'Guinea-Bissau' },
        { value: 'Guyana', viewValue: 'Guyana' },
        { value: 'Haití', viewValue: 'Haití' },
        { value: 'Honduras', viewValue: 'Honduras' },
        { value: 'Hong Kong', viewValue: 'Hong Kong' },
        { value: 'Hungría', viewValue: 'Hungría' },
        { value: 'India', viewValue: 'India' },
        { value: 'Indonesia', viewValue: 'Indonesia' },
        { value: 'Irán', viewValue: 'Irán' },
        { value: 'Iraq', viewValue: 'Iraq' },
        { value: 'Irlanda', viewValue: 'Irlanda' },
        { value: 'Isla Bouvet', viewValue: 'Isla Bouvet' },
        { value: 'Isla de Navidad', viewValue: 'Isla de Navidad' },
        { value: 'Isla Norfolk', viewValue: 'Isla Norfolk' },
        { value: 'Islandia', viewValue: 'Islandia' },
        { value: 'Islas Åland', viewValue: 'Islas Åland' },
        { value: 'Islas Caimán', viewValue: 'Islas Caimán' },
        { value: 'Islas Cocos', viewValue: 'Islas Cocos' },
        { value: 'Islas Cook', viewValue: 'Islas Cook' },
        { value: 'Islas Feroe', viewValue: 'Islas Feroe' },
        { value: 'Islas Georgias del Sur y Sandwich del Sur', viewValue: 'Islas Georgias del Sur y Sandwich del Sur' },
        { value: 'Islas Heard y McDonald', viewValue: 'Islas Heard y McDonald' },
        { value: 'Islas Malvinas', viewValue: 'Islas Malvinas' },
        { value: 'Islas Marianas del Norte', viewValue: 'Islas Marianas del Norte' },
        { value: 'Islas Marshall', viewValue: 'Islas Marshall' },
        { value: 'Islas Pitcairn', viewValue: 'Islas Pitcairn' },
        { value: 'Islas Salomón', viewValue: 'Islas Salomón' },
        { value: 'Islas Turcas y Caicos', viewValue: 'Islas Turcas y Caicos' },
        { value: 'Islas ultramarinas de Estados Unidos', viewValue: 'Islas ultramarinas de Estados Unidos' },
        { value: 'Islas Vírgenes Británicas', viewValue: 'Islas Vírgenes Británicas' },
        { value: 'Islas Vírgenes de los Estados Unidos', viewValue: 'Islas Vírgenes de los Estados Unidos' },
        { value: 'Israel', viewValue: 'Israel' },
        { value: 'Italia', viewValue: 'Italia' },
        { value: 'Jamaica', viewValue: 'Jamaica' },
        { value: 'Japón', viewValue: 'Japón' },
        { value: 'Jordania', viewValue: 'Jordania' },
        { value: 'Kazajstán', viewValue: 'Kazajstán' },
        { value: 'Kenia', viewValue: 'Kenia' },
        { value: 'Kirguistán', viewValue: 'Kirguistán' },
        { value: 'Kiribati', viewValue: 'Kiribati' },
        { value: 'Kuwait', viewValue: 'Kuwait' },
        { value: 'Laos', viewValue: 'Laos' },
        { value: 'Lesotho', viewValue: 'Lesotho' },
        { value: 'Letonia', viewValue: 'Letonia' },
        { value: 'Líbano', viewValue: 'Líbano' },
        { value: 'Liberia', viewValue: 'Liberia' },
        { value: 'Libia', viewValue: 'Libia' },
        { value: 'Liechtenstein', viewValue: 'Liechtenstein' },
        { value: 'Lituania', viewValue: 'Lituania' },
        { value: 'Luxemburgo', viewValue: 'Luxemburgo' },
        { value: 'Macao', viewValue: 'Macao' },
        { value: 'Madagascar', viewValue: 'Madagascar' },
        { value: 'Malasia', viewValue: 'Malasia' },
        { value: 'Malawi', viewValue: 'Malawi' },
        { value: 'Maldivas', viewValue: 'Maldivas' },
        { value: 'Malí', viewValue: 'Malí' },
        { value: 'Malta', viewValue: 'Malta' },
        { value: 'Marruecos', viewValue: 'Marruecos' },
        { value: 'Martinica', viewValue: 'Martinica' },
        { value: 'Mauricio', viewValue: 'Mauricio' },
        { value: 'Mauritania', viewValue: 'Mauritania' },
        { value: 'Mayotte', viewValue: 'Mayotte' },
        { value: 'México', viewValue: 'México' },
        { value: 'Micronesia', viewValue: 'Micronesia' },
        { value: 'Moldavia', viewValue: 'Moldavia' },
        { value: 'Mónaco', viewValue: 'Mónaco' },
        { value: 'Mongolia', viewValue: 'Mongolia' },
        { value: 'Montserrat', viewValue: 'Montserrat' },
        { value: 'Mozambique', viewValue: 'Mozambique' },
        { value: 'Myanmar', viewValue: 'Myanmar' },
        { value: 'Namibia', viewValue: 'Namibia' },
        { value: 'Nauru', viewValue: 'Nauru' },
        { value: 'Nepal', viewValue: 'Nepal' },
        { value: 'Nicaragua', viewValue: 'Nicaragua' },
        { value: 'Níger', viewValue: 'Níger' },
        { value: 'Nigeria', viewValue: 'Nigeria' },
        { value: 'Niue', viewValue: 'Niue' },
        { value: 'Noruega', viewValue: 'Noruega' },
        { value: 'Nueva Caledonia', viewValue: 'Nueva Caledonia' },
        { value: 'Nueva Zelanda', viewValue: 'Nueva Zelanda' },
        { value: 'Omán', viewValue: 'Omán' },
        { value: 'Países Bajos', viewValue: 'Países Bajos' },
        { value: 'Pakistán', viewValue: 'Pakistán' },
        { value: 'Palau', viewValue: 'Palau' },
        { value: 'Palestina', viewValue: 'Palestina' },
        { value: 'Panamá', viewValue: 'Panamá' },
        { value: 'Papúa Nueva Guinea', viewValue: 'Papúa Nueva Guinea' },
        { value: 'Paraguay', viewValue: 'Paraguay' },
        { value: 'Perú', viewValue: 'Perú' },
        { value: 'Polinesia Francesa', viewValue: 'Polinesia Francesa' },
        { value: 'Polonia', viewValue: 'Polonia' },
        { value: 'Portugal', viewValue: 'Portugal' },
        { value: 'Puerto Rico', viewValue: 'Puerto Rico' },
        { value: 'Qatar', viewValue: 'Qatar' },
        { value: 'Reino Unido', viewValue: 'Reino Unido' },
        { value: 'República Centroafricana', viewValue: 'República Centroafricana' },
        { value: 'República Checa', viewValue: 'República Checa' },
        { value: 'República Democrática del Congo', viewValue: 'República Democrática del Congo' },
        { value: 'República Dominicana', viewValue: 'República Dominicana' },
        { value: 'Reunión', viewValue: 'Reunión' },
        { value: 'Ruanda', viewValue: 'Ruanda' },
        { value: 'Rumania', viewValue: 'Rumania' },
        { value: 'Rusia', viewValue: 'Rusia' },
        { value: 'Sahara Occidental', viewValue: 'Sahara Occidental' },
        { value: 'Samoa', viewValue: 'Samoa' },
        { value: 'Samoa Americana', viewValue: 'Samoa Americana' },
        { value: 'San Cristóbal y Nevis', viewValue: 'San Cristóbal y Nevis' },
        { value: 'San Marino', viewValue: 'San Marino' },
        { value: 'San Pedro y Miquelón', viewValue: 'San Pedro y Miquelón' },
        { value: 'San Vicente y las Granadinas', viewValue: 'San Vicente y las Granadinas' },
        { value: 'Santa Helena', viewValue: 'Santa Helena' },
        { value: 'Santa Lucía', viewValue: 'Santa Lucía' },
        { value: 'Santo Tomé y Príncipe', viewValue: 'Santo Tomé y Príncipe' },
        { value: 'Senegal', viewValue: 'Senegal' },
        { value: 'Serbia y Montenegro', viewValue: 'Serbia y Montenegro' },
        { value: 'Seychelles', viewValue: 'Seychelles' },
        { value: 'Sierra Leona', viewValue: 'Sierra Leona' },
        { value: 'Singapur', viewValue: 'Singapur' },
        { value: 'Siria', viewValue: 'Siria' },
        { value: 'Somalia', viewValue: 'Somalia' },
        { value: 'Sri Lanka', viewValue: 'Sri Lanka' },
        { value: 'Suazilandia', viewValue: 'Suazilandia' },
        { value: 'Sudáfrica', viewValue: 'Sudáfrica' },
        { value: 'Sudán', viewValue: 'Sudán' },
        { value: 'Suecia', viewValue: 'Suecia' },
        { value: 'Suiza', viewValue: 'Suiza' },
        { value: 'Surinam', viewValue: 'Surinam' },
        { value: 'Svalbard y Jan Mayen', viewValue: 'Svalbard y Jan Mayen' },
        { value: 'Tailandia', viewValue: 'Tailandia' },
        { value: 'Taiwán', viewValue: 'Taiwán' },
        { value: 'Tanzania', viewValue: 'Tanzania' },
        { value: 'Tayikistán', viewValue: 'Tayikistán' },
        { value: 'Territorio Británico del Océano Índico', viewValue: 'Territorio Británico del Océano Índico' },
        { value: 'Territorios Australes Franceses', viewValue: 'Territorios Australes Franceses' },
        { value: 'Timor Oriental', viewValue: 'Timor Oriental' },
        { value: 'Togo', viewValue: 'Togo' },
        { value: 'Tokelau', viewValue: 'Tokelau' },
        { value: 'Tonga', viewValue: 'Tonga' },
        { value: 'Trinidad y Tobago', viewValue: 'Trinidad y Tobago' },
        { value: 'Túnez', viewValue: 'Túnez' },
        { value: 'Turkmenistán', viewValue: 'Turkmenistán' },
        { value: 'Turquía', viewValue: 'Turquía' },
        { value: 'Tuvalu', viewValue: 'Tuvalu' },
        { value: 'Ucrania', viewValue: 'Ucrania' },
        { value: 'Uganda', viewValue: 'Uganda' },
        { value: 'Uruguay', viewValue: 'Uruguay' },
        { value: 'Uzbekistán', viewValue: 'Uzbekistán' },
        { value: 'Vanuatu', viewValue: 'Vanuatu' },
        { value: 'Venezuela', viewValue: 'Venezuela' },
        { value: 'Vietnam', viewValue: 'Vietnam' },
        { value: 'Wallis y Futuna', viewValue: 'Wallis y Futuna' },
        { value: 'Yemen', viewValue: 'Yemen' },
        { value: 'Yibuti', viewValue: 'Yibuti' },
        { value: 'Zambia', viewValue: 'Zambia' },
        { value: 'Zimbabue', viewValue: 'Zimbabue' },
    ]
}
