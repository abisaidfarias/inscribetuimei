import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../md/md.module';
import { MaterialModule } from '../app.module';

import { FormularioComponent } from './formulario.component';
import { FormularioRoutes } from './formulario.routing';
import { FormularioService } from 'app/formulario/formulario.service';
import { Globals } from 'app/globals';
import { RecaptchaModule } from 'ng-recaptcha';
import { FieldErrorDisplayComponent } from 'app/forms/validationforms/field-error-display/field-error-display.component';
import { Constant } from 'app/constant';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(FormularioRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        ReactiveFormsModule,
        RecaptchaModule.forRoot()
    ],
    declarations: [FormularioComponent, FieldErrorDisplayComponent],
    providers: [FormularioService, Globals,Constant]
})

export class FormularioModule { }
