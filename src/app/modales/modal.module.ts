import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';

import { MatDialogModule, MatFormField, MatInputModule } from '@angular/material';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [
        CommonModule,
        MatDialogModule,
        NgSelectModule,
        MatFormField,
        MatInputModule,
        FormsModule       
    ],
    declarations: []
})

export class ModalModule { }
