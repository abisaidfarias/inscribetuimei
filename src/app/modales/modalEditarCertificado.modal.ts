/* import { Component, OnInit, Inject, ElementRef } from "@angular/core";
import { EmpresasService } from "app/empresas/empresas.service";
import { Router } from "@angular/router";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { isNumeric } from 'rxjs/util/isNumeric';
import { CertificadosService } from "app/certificados/certificados.service";


declare var swal: any;
@Component({
    selector: 'app-modal-certificado',
    templateUrl: 'modalEditarCertificado.modal.html',
})
export class ModalEditarCertificadoComponent implements OnInit {
    ngOnInit(): void {
        this.getEmpresas();
        this.getMarcas();
    }
    constructor(private router: Router, private elem: ElementRef,
         public dialogRef: MatDialogRef<ModalEditarCertificadoComponent>,
        private empresasService: EmpresasService,
        private certificadoService: CertificadosService,
        @Inject(MAT_DIALOG_DATA) public certificado: any) { }
    maxValueSar = 1.60;
    empresas: any[];
    marcas:any[];
    archivo: string = "";
    archivoListo: boolean = false;
    loadingFile: string = "subir";
    etiquetas = [
        { Id: 0, Nombre: '2G 3G 4G' },
        { Id: 1, Nombre: '2G' },
        { Id: 2, Nombre: '3G' },
        { Id: 3, Nombre: '4G' },
        { Id: 4, Nombre: '2G 3G' },
        { Id: 5, Nombre: '3G 4G' },
        { Id: 6, Nombre: '2G 4G' }
    ];
    validarSAR() {
        if (!isNumeric(this.certificado.ValorSAR)) {
            this.certificado.ValorSAR = '';
            return;
        }
        if (this.certificado.ValorSAR > this.maxValueSar) {

            this.certificado.ValorSAR = '';
        }


    }
    getMarcas() {
        this.empresasService.getMarcas()
            .subscribe(
            (servers: any) => {
                this.marcas = servers;
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Hubo un error cargando las Empresas</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }

    getEmpresas() {
        this.empresasService.getEmpresasByTipo()
            .subscribe(
            (servers: any) => {
                this.empresas = servers.Empresas;
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Hubo un error cargando las Empresas</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }
    fileEvent(files) {
        this.loadingFile = 'subir';
        this.archivo = files[0].name;
        this.archivoListo = true;
    }
    subirArchivoCertificado() {
        this.loadingFile = 'subiendo';
        let files = this.elem.nativeElement.querySelector('#selectedFile').files;
        let formData = new FormData();
        let file = files[0];
        formData.append('selectedFile', file, file.name);
        this.certificadoService.subirCertificado(formData)
            .subscribe((servers: any) => {
                this.loadingFile = 'subido';
                this.certificado.FileId = servers.Id;
            },
            (error) => {
                this.loadingFile = 'subir';
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Error en la Operación</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });
    }

 } */