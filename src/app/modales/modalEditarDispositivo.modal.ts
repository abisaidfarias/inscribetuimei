/* import { Component, OnInit, Inject } from "@angular/core";
import { EmpresasService } from "app/empresas/empresas.service";
import { Router } from "@angular/router";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DispositivoService } from "app/otro-dispositivo/otro-dispositivo.service";

declare var swal: any;
@Component({
    selector: 'app-modal-dispositivo',
    templateUrl: 'modalEditarDispositivo.modal.html',
})
export class ModalEditarDispositivoComponent implements OnInit {

    ngOnInit(): void {
        this.getEmpresas();
        this.getMarcas();
        this.getTipoDispositivos();
    }
    constructor(private router: Router, private dispositivoService:DispositivoService,
        private empresasService: EmpresasService, public dialogRef: MatDialogRef<ModalEditarDispositivoComponent>,
        @Inject(MAT_DIALOG_DATA) public dispositivo: any) { }
    
    empresas: any[];
    marcas:any[];
    tipos:any[];
    

    getTipoDispositivos() {
        this.dispositivoService.getTipoDispositivo()
            .subscribe(
            (servers: any) => {
                this.tipos = servers;
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Hubo un error cargando las Empresas</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }
    getMarcas() {
        this.empresasService.getMarcas()
            .subscribe(
            (servers: any) => {
                this.marcas = servers;
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Hubo un error cargando las Empresas</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }

    getEmpresas() {
        this.empresasService.getEmpresasByTipo()
            .subscribe(
            (servers: any) => {
                this.empresas = servers.Empresas;
            },
            (error) => {
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Hubo un error cargando las Empresas</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }


 } */