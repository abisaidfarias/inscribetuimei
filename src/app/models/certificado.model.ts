export interface Certificado {
  Id: number;
  NombreEmpresa: string;
  Marca: string;
  FechaControlView: string;
  ModeloTecnico: string;
  VersionSoftwareConcesionario: string;
  VersionSoftwareFabricante: string;
  NumeroCertificacion: string;
  ModeloComercial: string;
  VersionSistemaOperativo: string;
  ValorSAR: string;
  SistemaOperativo: string;
  EtiquetaView: string;
  IdOABI: string;
  VersionHardware: string;
  FechaControlAno: string;
  NumeroCertificacionEdit: string;
  FechaControl: Date;
}