import { Injectable } from '@angular/core';

@Injectable()
export class Constant {
    TituloEs: string ="Formulario de Inscripción Administrativa";
    TituloEn: string = "Administrative registration form";
    NacionalidadEs: string = "Nacionalidad";
    NacionalidadEn: string = "Nacionality";
    RutoPasaporteEs: string = "Rut/Pasaporte o DNI";
    RutoPasaporteEn: string = "Rut/Passport or DNI";
    NombreEs: string = "Nombre y Apellido";
    NombreEn: string = "Name and Lastname";
    TelefonoEs: string = "Teléfono de Contacto";
    TelefonoEn: string = "Contact Phone";
    MarcaEs: string = "Marca";
    MarcaEn: string = "Brand";
    ModelEs: string = "Modelo";
    ModelEn: string = "Model";
    PaisOrigenEs: string = "Pais Origen del Teléfono";
    PaisOrigenEn: string = "Country Origin of the Telephone";
    LugarEs: string = "Lugar de Compra";
    LugarEn: string = "Country Origin of the Telephone";
    LugarDescripcionEs: string = "Ej: Mi Teléfono fue adquirido en Amazon";
    LugarDescripcionEn: string = "Ex: My Phone was purchased at Amazon";
    ImageRutPasaporteEs: string = "Rut por ambos lados";
    ImageRutPasaporteEn: string = "Rut on both sides";
    ImageImeiElectronicoEs: string = "IMEI Electrónico o Fisico";
    ImageImeiElectronicoEn: string = "Electronic IMEI";
    ImageImeiFisicoEs: string = "IMEI Fisico";
    ImageImeiFisicoEn: string = "Physical IMEI";
    ImageFacturaEs: string = "Boleta o Factura";
    ImageFacturaEn: string = "Purchase invoice";
    ImageTimbradoEs: string = "Pasaporte Timbrado";
    ImageTimbradoEn: string = "Stamped passport";
    AcuedoServicioEs: string = "Estoy de acuerdo con los términos del servicio";
    AcuedoServicioEn: string = "I agree to the terms of service";
    CostoServicio1Es: string = "Referente a la gratuidad.";
    CostoServicio2Es: string = "P: ¿Esta inscripción tiene costo?";
    CostoServicio3Es: string = "R: No tiene costo, siempre y cuando no sea más de un (1) equipo por persona natural al año (365 días continuos desde su ultima inscripción) , y siempre y cuando nuestra Empresa Certificadora no supere el 2% de los registros(de IMEIs) anuales ingresados al sistema, en cuyo caso te aconsejamos recurrir a otra Empresa Certificadora que no haya superado dicho límite.";
    CostoServicio4Es: string = "Las inscripciones adicionales tienen un costo de $10.000.";
    CostoServicio5Es: string = "Medios de pagos disponibles:";
    CostoServicio6Es: string = "* Transferencia Bancaria.";
    CostoServicio1En: string = "Regarding the gratuity.";
    CostoServicio2En: string = "Q: Does this registration have a cost?";
    CostoServicio3En: string = "A: It has no cost, as long as it is not more than one (1) team per natural person per year (365 days continuous since your last registration), and as long as our Certification Company does not exceed 2% of the annual records(of IMEIs) entered into the system, in which case we advise you to resort to another Certifying Company that has not exceeded this limit.";
    CostoServicio4En: string = "Additional registrations cost $ 10,000.";
    CostoServicio5En: string = "Payment methods available:";
    CostoServicio6En: string = "* Wire transfer.";
    ImagenAdjuntarEs: string = "Adjuntar Imagen";
    ImagenAdjuntarEn: string = "Attach Image";
    ImagenEliminarEs: string = "Eliminar";
    ImagenEliminarEn: string = "Delete";
    ImagenCambiarEs: string = "Cambiar";
    ImagenCambiarEn: string = "Change";


}