import { Routes } from '@angular/router';
import { AuthGuard } from 'app/auth/auth';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'imeiInfo/imeiInfo',
        pathMatch: 'full',
        canActivate: [AuthGuard]
    }
    , {
        path: '',
        children: [
            {
                path: '',
                loadChildren: './formulario/formulario.module#FormularioModule',
            },
            {
                path: 'imeiInfo',
                loadChildren: './imeiInfo/imeiInfo.module#ImeiInfoModule',
                canActivate: [AuthGuard]
            }
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule',
        }]
    }
];
