import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../app.module';

import { UsuariosRoutes } from './user.routing';

import { ListarUsuariosComponent } from './listar-usuarios/listar-usuarios.component';
import { MensajeErrorComponent } from './mensaje-error/mensaje-error.component';
import { UserService } from 'app/user/user.service';
import { Globals } from 'app/globals';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(UsuariosRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        Ng2SmartTableModule,
        NgSelectModule
    ],
    declarations: [
        ListarUsuariosComponent,
        MensajeErrorComponent
    ],
    providers: [ UserService, Globals]
})

export class UsuariosModule { }
