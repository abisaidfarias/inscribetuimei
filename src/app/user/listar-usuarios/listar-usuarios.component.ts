import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from 'app/user/user.service';

declare var swal: any;
declare const $: any;

@Component({
    selector: 'app-listar-usuarios',
    templateUrl: 'listar-usuarios.component.html'
})

export class ListarUsuariosComponent implements OnInit {
    public data: Object;
    public loading = true;

    constructor(private router: Router, private userService: UserService){}

    ngOnInit(): void {
        this.get();
    }

    settings = {
        defaultStyle: false,
        attr: {
            class: 'table-striped' // this is custom table scss or css class for table
        },
        actions: false,
        columns: {
            Nombre: {
                title: 'Nombre',
                width: '100px'
            },
            Apellido: {
                title: 'Apellido',
                width: '180px'
            },
            Usuario: {
                title: 'Usuario',
                width: '120px'
            },
            FechaCreacion: {
                title: 'Fecha de Creación',
                width: '180px'
            },
            Empresa: {
                title: 'Cliente',
                width: '100px'
            }
        }
    };
    get() {
        this.userService.getUsers()
            .subscribe(
            (servers: any) => {
                this.data = servers;
                this.loading = false;
            },
            (error) => {
                this.loading = false;
                if (error.status == 401) {
                    this.router.navigate(['pages/login']);
                }
                else {
                    swal({
                        type: 'error',
                        html: '<strong>Error en la Operación</strong>',
                        confirmButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    });
                }
            });

    }
}
