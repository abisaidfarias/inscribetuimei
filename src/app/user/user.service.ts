import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Globals } from 'app/globals';


@Injectable()
export class UserService {

    constructor(private http: Http, private router: Router, private globals: Globals) { }
    //GET
    getUsers(): Observable<boolean> {
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        const headers = new Headers(
            {
                "Content-Type": "application/json; charset=utf-8",
                "Accept": "application/json; charset=utf-8",
                "Authorization": "Bearer " + curentUser.token
            });

        return this.http.get(this.globals.urlpathApi + 'Users', { headers: headers })
            .map((response: Response) => { return response.json() });

    }
    //DELETE
    
    //POST
    crearUsuario(usuario: any): Observable<boolean> {
        let curentUser = JSON.parse(localStorage.getItem('currentUser'));
        let body = 'Email=' + usuario.email + '&Password=' + usuario.password +
            '&Nombre=' + usuario.nombre + '&Apellido=' + usuario.apellido + '&EmpresaId=' + usuario.empresaId;
        let cabecera = new Headers();
        cabecera.append('Content-Type', 'application/x-www-form-urlencoded');

        let options = new RequestOptions({ headers: cabecera });

        return this.http.post(this.globals.urlpathApi + 'Account/Register', body, options)
            .map((response: Response) => {
                return true;
            });
    }
    //PUT
   

}