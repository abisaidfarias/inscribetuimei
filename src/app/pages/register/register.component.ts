import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { PasswordValidation } from 'app/forms/validationforms/password-validator.component';
import { AuthenticationService } from 'app/services/authentication.service';

declare var swal: any;
declare interface ValidatorFn {
    (c: AbstractControl): {
        [key: string]: any;
    };
}

@Component({
    selector: 'app-recuperar',
    templateUrl: './register.component.html'
})

export class RegisterComponent implements OnInit {
    recuperar: FormGroup;
    public confirmCheck=false;
    public emailCheck = true;
    public passwordCheck = false;
    public recuperarInput: any = {};
    public emailIni="";
    loading = false;
    
    

    constructor(private activatedRoute: ActivatedRoute,private authenticationService: AuthenticationService,private router: Router, private formBuilder: FormBuilder)
    {}
    ngOnInit() {
        this.initRegister();
        this.activatedRoute.queryParams
            .subscribe(params => {
                this.emailIni=params.email;
                localStorage.setItem("code",params.code);
            });
    }

    initRegister() {
        this.recuperar = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required]
        },
            {
                validator: PasswordValidation.MatchPassword // your validation method
            });
    }
    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({ onlySelf: true });
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }

    restaurarClave() {
        if (this.recuperar.valid) {
            this.loading = true;
            this.recuperarInput = this.recuperar.value;
            this.recuperarInput.Code = localStorage.getItem("code");
            this.authenticationService.restaurarClave(this.recuperarInput)
            .subscribe((servers: any) => {
                this.loading = false;
                swal({
                    type: 'success',
                    html: '<strong>Operación Exitosa</strong>',
                    confirmButtonClass: 'btn btn-success',
                    buttonsStyling: false
                });
                this.router.navigate(['pages/login']);
            },
            (error) => {
                this.loading = false;
                swal({
                    type: 'error',
                    html: '<strong>Error en la Operación</strong>',
                    confirmButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                });
            });
        } else {
            this.validateAllFormFields(this.recuperar);
        } 
    }
    isFieldValid(form: FormGroup, field: string) {
        let value = !form.get(field).valid && form.get(field).touched;
        if (form.controls.confirmPassword.status =='VALID'){
            this.confirmCheck=true;
        }
        else
        {
            this.confirmCheck = false;
        }
        if (form.controls.password.status == 'VALID') {
            this.passwordCheck=true;
        }
        else{
            this.passwordCheck = false;
        }
        if (form.controls.email.status == 'VALID') {
            this.emailCheck=true;
        }    
        else{
            this.emailCheck = false;
        }
        return value;
    }

    displayFieldCss(form: FormGroup, field: string) {
        return {
            'has-error': this.isFieldValid(form, field),
            'has-feedback': this.isFieldValid(form, field)
        };
    }
}
