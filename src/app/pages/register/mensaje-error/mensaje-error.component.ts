import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-mensaje-recuperar-error',
    templateUrl: './mensaje-error.component.html',
    styleUrls: ['./mensaje-error.component.css']

})
export class MensajeErrorRecuperarComponent {

    @Input() errorMsg: string;
    @Input() displayError: boolean;

}
