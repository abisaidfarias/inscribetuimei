import { Component, OnInit, ElementRef } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { AlertService } from 'app/services/alert.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $: any;
declare var swal: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    public usuario: any = {};
    test: Date = new Date();
    private toggleButton: any;
    private sidebarVisible: boolean;
    private nativeElement: Node;
    private alertService: AlertService;
    loading = false;
    errorMessage: string = "";
    public emailOlvido="";
    public esOlvidoClave=false;
    public estilo=true;
    returnUrl: string;
    private route: ActivatedRoute;

    constructor(private element: ElementRef,
        private authenticationService: AuthenticationService,
        private router: Router) {
        this.nativeElement = element.nativeElement;
        this.sidebarVisible = false;

    }

    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);
        this.returnUrl = '/';
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    habilitarEstilo(){
        this.estilo = false;
    }
    login() {
        if(this.usuario.User && this.usuario.Password)
        {
            this.loading = true;
            this.errorMessage = "";
            return this.authenticationService.login(this.usuario.User, this.usuario.Password)
                .subscribe(
                data => {
                    this.router.navigate(['imeiInfo/imeiInfo']);
                },
                error => {
                    this.loading = false;    
                    this.errorMessage = "El usuario o la clave son incorrectos.";
                });                        
        }
    }
    olvidoClave() {
        this.loading = true;
        this.errorMessage = "";
        if(this.emailOlvido)
        {
            this.authenticationService.olvidoClave(this.emailOlvido)
            .subscribe((servers: any) => {
                this.loading = false;
                this.emailOlvido="";
                swal({
                    type: 'success',
                    html: '<strong>Se ha enviado un link a su correo para recuperar su clave, por favor revíselo.</strong>',
                    confirmButtonClass: 'btn btn-success',
                    buttonsStyling: false
                });
                this.esOlvidoClave = false;
                this.router.navigate(['pages/login']);
            },
            (error) => {
                this.loading = false;
                swal({
                    type: 'error',
                    html: '<strong>Error en la Operación</strong>',
                    confirmButtonClass: 'btn btn-danger',
                    buttonsStyling: false
                });
            });
        }

    }
    activarOlvidoClave(esOlvido){

        if(esOlvido){
            this.esOlvidoClave = true;
        }else{
            this.esOlvidoClave = false;
        }
        
    }

}
