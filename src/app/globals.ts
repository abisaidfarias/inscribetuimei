import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
    urlpathApi: string = "https://api.cloud-lbtechnology.com/api/";
    urlpath: string = "https://api.cloud-lbtechnology.com/";
/*     urlpathApi: string = "http://localhost:56242/api/";
    urlpath: string = "http://localhost:56242/"; */
}